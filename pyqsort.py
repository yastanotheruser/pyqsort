def __defaultcmpfn(a, b):
    return a - b

def __qsort(list, start, end, cmpfn = __defaultcmpfn):
    delta = end - start
    if delta <= 1:
        return

    pivindex = start + delta // 2
    pivot = list[pivindex]

    i = start
    while i < end:
        cmp = cmpfn(list[i], pivot)
        if i < pivindex and not cmp <= 0:
            list.insert(end - 1, list.pop(i))
            pivindex -= 1
            i -= 1
        elif i > pivindex and not cmp > 0:
            list.insert(start, list.pop(i))
            pivindex += 1
            i -= 1

        i += 1

    __qsort(list, start, pivindex, cmpfn)
    __qsort(list, pivindex + 1, end, cmpfn)

def qsort(list, cmpfn = __defaultcmpfn):
    __qsort(list, 0, len(list), cmpfn)