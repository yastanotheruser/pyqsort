from pyqsort.pyqsort import qsort
from random import random

def qsort_random(len = 20, inf = -10, sup = 20, cmpfn = lambda a, b: a - b):
    delta = sup - inf
    randlist = [inf + round(delta * random()) for i in range(len)]
    print("Unsorted random list:", randlist)
    qsort(randlist, cmpfn)
    print("Sorted list:", randlist)